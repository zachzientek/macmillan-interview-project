package com.macmillan.moviemanager.model.entity;

import java.time.Year;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long movieId;
    @NotBlank
    @Size(max = 250)
    private String title;
    @NotNull
    @ManyToOne
    @JoinColumn
    private Genre genre;
    @NotNull
    private Year year;
    @NotNull
    @ManyToOne
    @JoinColumn
    private Rating rating;

    public Movie() {}

    public Movie(String name, Genre genre, Year year, Rating rating) {
        this.title = name;
        this.genre = genre;
        this.year = year;
        this.rating = rating;
    }

    public Long getMovieId() {
        return this.movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Genre getGenre() {
        if(this.genre == null) {
            this.genre = new Genre();
        }
        return this.genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Year getYear() {
        return this.year;
    }

    public void setYear(Year year) {
        this.year = year;
    }

    public Rating getRating() {
        return this.rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.genre == null) ? 0 : this.genre.hashCode());
        result = prime * result + ((this.movieId == null) ? 0 : this.movieId.hashCode());
        result = prime * result + ((this.title == null) ? 0 : this.title.hashCode());
        result = prime * result + ((this.rating == null) ? 0 : this.rating.hashCode());
        result = prime * result + ((this.year == null) ? 0 : this.year.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Movie other = (Movie) obj;
        if (this.genre == null) {
            if (other.genre != null)
                return false;
        } else if (!this.genre.equals(other.genre))
            return false;
        if (this.movieId == null) {
            if (other.movieId != null)
                return false;
        } else if (!this.movieId.equals(other.movieId))
            return false;
        if (this.title == null) {
            if (other.title != null)
                return false;
        } else if (!this.title.equals(other.title))
            return false;
        if (this.rating != other.rating)
            return false;
        if (this.year == null) {
            if (other.year != null)
                return false;
        } else if (!this.year.equals(other.year))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Movie [movieId=" + this.movieId + ", name=" + this.title + ", genre=" + this.genre + ", year=" + this.year + ", rating=" + this.rating
                + "]";
    }

}
