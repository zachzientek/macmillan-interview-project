package com.macmillan.moviemanager.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
public class Genre {
    @Id
    @GeneratedValue
    private Long genreId;
    @NotBlank
    @Size(max = 250)
    @Column(unique = true)
    private String name;
    
    public Genre() {}

    public Genre(String name) {
        this.name = name;
    }

    public Long getGenreId() {
        return this.genreId;
    }

    public void setGenreId(Long genreId) {
        this.genreId = genreId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.genreId == null) ? 0 : this.genreId.hashCode());
        result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Genre other = (Genre) obj;
        if (this.genreId == null) {
            if (other.genreId != null)
                return false;
        } else if (!this.genreId.equals(other.genreId))
            return false;
        if (this.name == null) {
            if (other.name != null)
                return false;
        } else if (!this.name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Genre [genreId=" + this.genreId + ", name=" + this.name + "]";
    }
}
