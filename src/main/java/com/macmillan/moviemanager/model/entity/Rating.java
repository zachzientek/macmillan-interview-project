package com.macmillan.moviemanager.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
public class Rating {
    @Id
    @GeneratedValue
    private Long ratingId;
    @NotBlank
    @Size(max = 250)
    @Column(unique=true)
    private String name;
    
    public Rating() {}

    public Rating(String name) {
        this.name = name;
    }

    public Long getRatingId() {
        return this.ratingId;
    }

    public void setRatingId(Long ratingId) {
        this.ratingId = ratingId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.ratingId == null) ? 0 : this.ratingId.hashCode());
        result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Rating other = (Rating) obj;
        if (this.ratingId == null) {
            if (other.ratingId != null)
                return false;
        } else if (!this.ratingId.equals(other.ratingId))
            return false;
        if (this.name == null) {
            if (other.name != null)
                return false;
        } else if (!this.name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Rating [ratingId=" + this.ratingId + ", name=" + this.name + "]";
    }
}
