package com.macmillan.moviemanager.service;

import com.macmillan.moviemanager.model.entity.Rating;

public interface RatingService {

    Iterable<Rating> readAll();

    Rating readById(long ratingId);

    Rating create(Rating rating);

    Rating update(Rating rating);

    void delete(long ratingId);

}
