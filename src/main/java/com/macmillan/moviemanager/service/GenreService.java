package com.macmillan.moviemanager.service;

import com.macmillan.moviemanager.model.entity.Genre;

public interface GenreService {

    Iterable<Genre> readAll();

    Genre readById(long genreId);

    Genre create(Genre genre);

    Genre update(Genre genre);

    void delete(long genreId);

}
