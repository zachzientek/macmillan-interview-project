package com.macmillan.moviemanager.service;

import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.macmillan.moviemanager.model.entity.Rating;
import com.macmillan.moviemanager.repository.RatingRepository;

@Service
public class RatingServiceImpl implements RatingService {
    private final RatingRepository ratingRepository;
    private final Validator validator;

    @Autowired
    public RatingServiceImpl(RatingRepository ratingRepository, Validator validator) {
        this.ratingRepository = ratingRepository;
        this.validator = validator;
    }

    @Override
    public Iterable<Rating> readAll() {
        return this.ratingRepository.findAll();
    }

    @Override
    public Rating readById(long ratingId) {
        return this.ratingRepository.findById(ratingId).get();
    }

    @Override
    public Rating create(Rating rating) {
        this.validator.validate(rating);
        return this.ratingRepository.save(rating);
    }

    @Override
    public Rating update(Rating rating) {
        this.validator.validate(rating);
        return this.ratingRepository.save(rating);
    }

    @Override
    public void delete(long ratingId) {
        this.ratingRepository.deleteById(ratingId);
    }

}
