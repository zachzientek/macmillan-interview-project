package com.macmillan.moviemanager.service;

import com.macmillan.moviemanager.model.entity.Movie;

public interface MovieService {

    Iterable<Movie> readAll();

    Movie readById(long movieId);

    Movie create(Movie movie);

    Movie update(Movie movie);

    void delete(long movieId);

}
