package com.macmillan.moviemanager.service;

import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.macmillan.moviemanager.model.entity.Movie;
import com.macmillan.moviemanager.repository.MovieRepository;

@Service
public class MovieServiceImpl implements MovieService {
    private final MovieRepository movieRepository;
    private final Validator validator;
    
    @Autowired      
    public MovieServiceImpl(MovieRepository movieRepository, Validator validator) {
        this.movieRepository = movieRepository;
        this.validator = validator;
    }


    @Override
    public Iterable<Movie> readAll() {
        return this.movieRepository.findAll();
    }
    
    @Override
    public Movie readById(long movieId) {
        return this.movieRepository.findById(movieId).get();
    }
    
    @Override
    public Movie create(Movie movie) {
        this.validator.validate(movie);
        
        return this.movieRepository.save(movie);
    }
    
    @Override
    public Movie update(Movie movie) {
        this.validator.validate(movie);
        
        return this.movieRepository.save(movie);
    }
    
    @Override
    public void delete(long movieId) {
        this.movieRepository.deleteById(movieId);
    }
}
