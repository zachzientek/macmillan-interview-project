package com.macmillan.moviemanager.service;

import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.macmillan.moviemanager.model.entity.Genre;
import com.macmillan.moviemanager.repository.GenreRepository;

@Service
public class GenreServiceImpl implements GenreService {
    private final GenreRepository genreRepository;
    private final Validator validator;

    @Autowired
    public GenreServiceImpl(GenreRepository genreRepository, Validator validator) {
        this.genreRepository = genreRepository;
        this.validator = validator;
    }

    @Override
    public Iterable<Genre> readAll() {
        return this.genreRepository.findAll();
    }

    @Override
    public Genre readById(long genreId) {
        return this.genreRepository.findById(genreId).get();
    }

    @Override
    public Genre create(Genre genre) {
        this.validator.validate(genre);
        return this.genreRepository.save(genre);
    }

    @Override
    public Genre update(Genre genre) {
        this.validator.validate(genre);
        return this.genreRepository.save(genre);
    }

    @Override
    public void delete(long genreId) {
        this.genreRepository.deleteById(genreId);
    }

}
