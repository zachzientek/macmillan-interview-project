package com.macmillan.moviemanager.repository;

import java.time.Year;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.macmillan.moviemanager.model.entity.Movie;

@Repository
public interface MovieRepository extends CrudRepository<Movie, Long> {

    Set<Movie> findByTitle(String title);
    
    Set<Movie> findByYear(Year year);
}
