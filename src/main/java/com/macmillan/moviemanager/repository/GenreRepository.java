package com.macmillan.moviemanager.repository;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.macmillan.moviemanager.model.entity.Genre;

@Repository
public interface GenreRepository extends CrudRepository<Genre, Long> {

    Set<Genre> findByName(String name);
}
