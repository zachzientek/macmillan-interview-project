package com.macmillan.moviemanager.repository;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.macmillan.moviemanager.model.entity.Rating;

@Repository
public interface RatingRepository extends CrudRepository<Rating, Long> {

    Set<Rating> findByName(String name);
}
