package com.macmillan.moviemanager.api.v1.converter.out;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.macmillan.moviemanager.model.entity.Rating;


@Component("com.macmillan.moviemanager.api.v1.converter.out.RatingConverter")
public class RatingConverter implements Converter<Rating, com.macmillan.moviemanager.model.api.v1.Rating> {

    @Override
    public com.macmillan.moviemanager.model.api.v1.Rating convert(Rating source) {
        if (source == null) {
            return null;
        }
        
        com.macmillan.moviemanager.model.api.v1.Rating target = new com.macmillan.moviemanager.model.api.v1.Rating();
        target.setRatingId(source.getRatingId());
        target.setName(source.getName());        
        return target;
    }

}
