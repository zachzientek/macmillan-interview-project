package com.macmillan.moviemanager.api.v1.converter.in;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.macmillan.moviemanager.model.api.v1.Movie;

@Component("com.macmillan.moviemanager.api.v1.converter.in.MovieConverter")
public class MovieConverter implements Converter<Movie, com.macmillan.moviemanager.model.entity.Movie> {
    private final GenreConverter genreConverter;
    private final RatingConverter ratingConverter;

    @Autowired
    public MovieConverter(GenreConverter genreConverter, RatingConverter ratingConverter) {
        this.genreConverter = genreConverter;
        this.ratingConverter = ratingConverter;
    }

    @Override
    public com.macmillan.moviemanager.model.entity.Movie convert(Movie source) {
        if (source == null) {
            return null;
        }

        com.macmillan.moviemanager.model.entity.Movie target = new com.macmillan.moviemanager.model.entity.Movie();
        target.setGenre(this.genreConverter.convert(source.getGenre()));
        target.setMovieId(source.getMovieId());
        target.setRating(this.ratingConverter.convert(source.getRating()));
        target.setTitle(source.getTitle());
        target.setYear(source.getYear());
        return target;
    }

}
