package com.macmillan.moviemanager.api.v1.converter.in;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.macmillan.moviemanager.model.api.v1.Genre;

@Component("com.macmillan.moviemanager.api.v1.converter.in.GenreConverter")
public class GenreConverter implements Converter<Genre, com.macmillan.moviemanager.model.entity.Genre> {

    @Override
    public com.macmillan.moviemanager.model.entity.Genre convert(Genre source) {
        if (source == null) {
            return null;
        }
        
        com.macmillan.moviemanager.model.entity.Genre target = new com.macmillan.moviemanager.model.entity.Genre();
        target.setGenreId(source.getGenreId());
        target.setName(source.getName());        
        return target;
    }

}
