package com.macmillan.moviemanager.api.v1.controller;

import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.macmillan.moviemanager.model.api.v1.Genre;
import com.macmillan.moviemanager.service.GenreService;

@RestController
@RequestMapping("/api/rest/v1/genres")
public class GenreController {
    private final GenreService genreService;
    private final ConversionService conversionService;
    
    @Autowired
    public GenreController(GenreService genreService, ConversionService conversionService) {
        this.genreService = genreService;
        this.conversionService = conversionService;
    }

    @GetMapping
    public ResponseEntity<Set<Genre>> readGenres() {
        Iterable<com.macmillan.moviemanager.model.entity.Genre> entities = this.genreService.readAll();
        Set<Genre> apis = StreamSupport.stream(entities.spliterator(), false).map(entity -> this.conversionService.convert(entity, Genre.class)).collect(Collectors.toSet());
        return ResponseEntity.ok().body(apis);
    }
    
    @GetMapping("/{genreId}")
    public ResponseEntity<Genre> readById(@PathVariable final long genreId) {
        com.macmillan.moviemanager.model.entity.Genre entity = this.genreService.readById(genreId);
        Genre api = this.conversionService.convert(entity, Genre.class);
        return ResponseEntity.ok().body(api);
    }
    
    @PostMapping
    public ResponseEntity<Genre> create(@RequestBody Genre api) {
        api.setGenreId(null);
        
        com.macmillan.moviemanager.model.entity.Genre entity = this.conversionService.convert(api, com.macmillan.moviemanager.model.entity.Genre.class);
        entity = this.genreService.create(entity);
        api = this.conversionService.convert(entity, Genre.class);
        
        return ResponseEntity.created(URI.create("/" + api.getGenreId())).body(api);
    }
    
    @PutMapping("/{genreId}")
    public ResponseEntity<Genre> update(@PathVariable final long genreId, @RequestBody Genre api) {
        api.setGenreId(genreId);
        
        com.macmillan.moviemanager.model.entity.Genre entity = this.conversionService.convert(api, com.macmillan.moviemanager.model.entity.Genre.class);
        entity = this.genreService.update(entity);
        api = this.conversionService.convert(entity, Genre.class);
        
        return ResponseEntity.ok().body(api);
    }

    @DeleteMapping(value = "/{genreId}")
    public ResponseEntity<?> delete(@PathVariable long genreId) {
        this.genreService.delete(genreId);
        
        return ResponseEntity.noContent().build();
    }

}
