package com.macmillan.moviemanager.api.v1.controller;

import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.macmillan.moviemanager.model.api.v1.Rating;
import com.macmillan.moviemanager.service.RatingService;

@RestController
@RequestMapping("/api/rest/v1/ratings")
public class RatingController {
    private final RatingService ratingService;
    private final ConversionService conversionService;
    
    @Autowired
    public RatingController(RatingService ratingService, ConversionService conversionService) {
        this.ratingService = ratingService;
        this.conversionService = conversionService;
    }

    @GetMapping
    public ResponseEntity<Set<Rating>> readRatings() {
        Iterable<com.macmillan.moviemanager.model.entity.Rating> entities = this.ratingService.readAll();
        Set<Rating> apis = StreamSupport.stream(entities.spliterator(), false).map(entity -> this.conversionService.convert(entity, Rating.class)).collect(Collectors.toSet());
        return ResponseEntity.ok().body(apis);
    }
    
    @GetMapping("/{ratingId}")
    public ResponseEntity<Rating> readById(@PathVariable final long ratingId) {
        com.macmillan.moviemanager.model.entity.Rating entity = this.ratingService.readById(ratingId);
        Rating api = this.conversionService.convert(entity, Rating.class);
        return ResponseEntity.ok().body(api);
    }
    
    @PostMapping
    public ResponseEntity<Rating> create(@RequestBody Rating api) {
        api.setRatingId(null);
        
        com.macmillan.moviemanager.model.entity.Rating entity = this.conversionService.convert(api, com.macmillan.moviemanager.model.entity.Rating.class);
        entity = this.ratingService.create(entity);
        api = this.conversionService.convert(entity, Rating.class);
        
        return ResponseEntity.created(URI.create("/" + api.getRatingId())).body(api);
    }
    
    @PutMapping("/{ratingId}")
    public ResponseEntity<Rating> update(@PathVariable final long ratingId, @RequestBody Rating api) {
        api.setRatingId(ratingId);
        
        com.macmillan.moviemanager.model.entity.Rating entity = this.conversionService.convert(api, com.macmillan.moviemanager.model.entity.Rating.class);
        entity = this.ratingService.update(entity);
        api = this.conversionService.convert(entity, Rating.class);
        
        return ResponseEntity.ok().body(api);
    }

    @DeleteMapping(value = "/{ratingId}")
    public ResponseEntity<?> delete(@PathVariable long ratingId) {
        this.ratingService.delete(ratingId);
        
        return ResponseEntity.noContent().build();
    }

}
