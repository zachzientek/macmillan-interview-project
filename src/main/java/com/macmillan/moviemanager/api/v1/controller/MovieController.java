package com.macmillan.moviemanager.api.v1.controller;

import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.macmillan.moviemanager.model.api.v1.Movie;
import com.macmillan.moviemanager.service.MovieService;

@RestController
@RequestMapping("/api/rest/v1/movies")
public class MovieController {
    private final MovieService movieService;
    private final ConversionService conversionService;
    
    @Autowired
    public MovieController(MovieService movieService, ConversionService conversionService) {
        this.movieService = movieService;
        this.conversionService = conversionService;
    }

    @GetMapping
    public ResponseEntity<Set<Movie>> readGenres() {
        Iterable<com.macmillan.moviemanager.model.entity.Movie> entities = this.movieService.readAll();
        Set<Movie> apis = StreamSupport.stream(entities.spliterator(), false).map(entity -> this.conversionService.convert(entity, Movie.class)).collect(Collectors.toSet());
        return ResponseEntity.ok().body(apis);
    }
    
    @GetMapping("/{movieId}")
    public ResponseEntity<Movie> readById(@PathVariable final long movieId) {
        com.macmillan.moviemanager.model.entity.Movie entity = this.movieService.readById(movieId);
        Movie api = this.conversionService.convert(entity, Movie.class);
        return ResponseEntity.ok().body(api);
    }
    
    @PostMapping
    public ResponseEntity<Movie> create(@RequestBody Movie api) {
        api.setMovieId(null);
        
        com.macmillan.moviemanager.model.entity.Movie entity = this.conversionService.convert(api, com.macmillan.moviemanager.model.entity.Movie.class);
        entity = this.movieService.create(entity);
        api = this.conversionService.convert(entity, Movie.class);
        
        return ResponseEntity.created(URI.create("/" + api.getMovieId())).body(api);
    }
    
    @PutMapping("/{movieId}")
    public ResponseEntity<Movie> update(@PathVariable final long movieId, @RequestBody Movie api) {
        api.setMovieId(movieId);
        
        com.macmillan.moviemanager.model.entity.Movie entity = this.conversionService.convert(api, com.macmillan.moviemanager.model.entity.Movie.class);
        entity = this.movieService.update(entity);
        api = this.conversionService.convert(entity, Movie.class);
        
        return ResponseEntity.ok().body(api);
    }

    @DeleteMapping("/{movieId}")
    public ResponseEntity<?> delete(@PathVariable long movieId) {
        this.movieService.delete(movieId);
        
        return ResponseEntity.noContent().build();
    }

}
