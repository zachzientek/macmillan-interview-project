package com.macmillan.moviemanager.api.v1.converter.out;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.macmillan.moviemanager.model.entity.Genre;


@Component("com.macmillan.moviemanager.api.v1.converter.out.GenreConverter")
public class GenreConverter implements Converter<Genre, com.macmillan.moviemanager.model.api.v1.Genre> {

    @Override
    public com.macmillan.moviemanager.model.api.v1.Genre convert(Genre source) {
        if (source == null) {
            return null;
        }
        
        com.macmillan.moviemanager.model.api.v1.Genre target = new com.macmillan.moviemanager.model.api.v1.Genre();
        target.setGenreId(source.getGenreId());
        target.setName(source.getName());        
        return target;
    }

}
