package com.macmillan.moviemanager.api.v1.controller;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/rest/v1/tests")
public class TestController {

    @GetMapping("/time")
    public ResponseEntity<String> readTime() {
        String nowAsString = ZonedDateTime.now(ZoneId.of("UTC")).toString();
        
        return ResponseEntity.ok().body(nowAsString);
    }   
}
