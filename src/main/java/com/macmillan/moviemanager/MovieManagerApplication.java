package com.macmillan.moviemanager;

import java.time.Year;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.macmillan.moviemanager.model.entity.Genre;
import com.macmillan.moviemanager.model.entity.Movie;
import com.macmillan.moviemanager.model.entity.Rating;
import com.macmillan.moviemanager.service.GenreService;
import com.macmillan.moviemanager.service.MovieService;
import com.macmillan.moviemanager.service.RatingService;

@SpringBootApplication
public class MovieManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MovieManagerApplication.class, args);
    }

    //@Bean
    public CommandLineRunner populateDatabase(RatingService ratingService, GenreService genreService, MovieService movieService) {
        return (args) -> {
            Map<String, Rating> ratings = this.populateRatings(ratingService);
            Map<String, Genre> genres = this.populateGenres(genreService);
            this.populateMovies(movieService, ratings, genres);
        };
    }
    
    private Map<String, Rating> populateRatings(RatingService ratingService) {
        List<String> ratings = Arrays.asList("G","PG","PG-13","R","NC-17","NR");
        return ratings.stream()
                      .map(rating -> ratingService.create(new Rating(rating)))
                      .collect(Collectors.toMap(Rating::getName, Function.identity()));
    }

    private Map<String, Genre> populateGenres(GenreService genreService) {
        List<String> genres = Arrays.asList(
                "Action",
                "Adventure",
                "Animation",
                "Biography",
                "Comedy",
                "Crime",
                "Documentary",
                "Drama",
                "Family",
                "Fantasy",
                "Film Noir",
                "History",
                "Horror",
                "Music",
                "Musical",
                "Mystery",
                "Romance",
                "Sci-Fi",
                "Short",
                "Sport",
                "Superhero",
                "Thriller",
                "War",
                "Western");
        return genres.stream()
                .map(genre -> genreService.create(new Genre(genre)))
                .collect(Collectors.toMap(Genre::getName, Function.identity()));        
    }

    private void populateMovies(MovieService movieService, Map<String, Rating> ratings, Map<String,Genre> genres) {
        movieService.create(new Movie("The Shawshank Redemption", genres.get("Drama"), Year.of(1994), ratings.get("R")));
        movieService.create(new Movie("Pulp Fiction", genres.get("Drama"), Year.of(1994), ratings.get("R")));
        movieService.create(new Movie("Inception", genres.get("Action"), Year.of(2010), ratings.get("PG-13")));
        movieService.create(new Movie("The Lion King", genres.get("Animation"), Year.of(1994), ratings.get("G")));
        movieService.create(new Movie("Back to the Future", genres.get("Comedy"), Year.of(1985), ratings.get("PG")));
    }
}
