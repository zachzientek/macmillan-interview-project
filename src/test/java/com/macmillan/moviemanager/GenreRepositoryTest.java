package com.macmillan.moviemanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.macmillan.moviemanager.model.entity.Genre;
import com.macmillan.moviemanager.repository.GenreRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class GenreRepositoryTest {

    @Autowired
    private GenreRepository genreRepository;

    @Test
    public void testGenreCreate() {
        Genre testGenre = new Genre("Test");
        this.genreRepository.save(testGenre);

        assertNotNull(testGenre.getGenreId());
    }

    @Test
    public void testGenreRead() {
        Genre testGenre = new Genre("Test");
        this.genreRepository.save(testGenre);
        Set<Genre> genres = this.genreRepository.findByName("Test");

        assertFalse(genres.isEmpty());
        assertTrue(genres.size() == 1);
        assertEquals("Test", genres.iterator().next().getName());
    }

    @Test
    public void testGenreUpdate() {
        Genre testGenre = new Genre("Test");
        this.genreRepository.save(testGenre);
        Long testGenreId = testGenre.getGenreId();
        
        testGenre.setName("Test2");
        this.genreRepository.save(testGenre);
        
        testGenre = this.genreRepository.findById(testGenreId).get();
        assertEquals("Test2", testGenre.getName());
    }
    
    @Test
    public void testGenreDelete() {
        Genre testGenre = new Genre("Test");
        this.genreRepository.save(testGenre);
        
        long count = this.genreRepository.count();
        assertEquals(1L, count);
        
        this.genreRepository.deleteById(testGenre.getGenreId());
        count = this.genreRepository.count();
        assertEquals(0L, count);
    }
}
