# README #

This application requires Java 11 and maven. It was built using Spring Boot 2.1.4, Spring Boot JPA, Spring Boot Web, and Spring Boot Test.

# Interaction #
* To build, execute: mvn clean install
* To run tests exclusively, execute: mvn test
* To run the application, execute: mvn spring-boot:run

# API Endpoints #
Root URL: http://localhost:8080

### Test ###
* GET /api/rest/v1/tests/time (current UTC time)

### Genre CRUD ###
* GET /api/rest/v1/genres (list of genres)
* POST /api/rest/v1/genres {genre} (create genre)
* PUT /api/rest/v1/genres/{genreId} {genre} (update genre)
* DELETE /api/rest/v1/genres/{genreId}

### Rating CRUD ###
* GET /api/rest/v1/ratings (list of ratings)
* POST /api/rest/v1/ratings {rating} (create rating)
* PUT /api/rest/v1/ratings/{ratingId} {rating} (update rating)
* DELETE /api/rest/v1/ratings/{ratingId}

### Movie CRUD ###
* GET /api/rest/v1/movies (list of movies)
* POST /api/rest/v1/movies {movie} (create movie)
* PUT /api/rest/v1/movies/{movieId} {movie} (update movie)
* DELETE /api/rest/v1/movies/{movieId}

# Application Structure #
This application was built to showcase a few typical enterprise design standards including

* Separation of functional layers (API, Service, Repository)
* A versioned API built ready to support backwards compatibility for future versions (package structure, converters, seperation of versioned API model from entity)
* RESTful API (distinct resource locations, consistent CRUD implementations for each resource, etc.)
* Normalized entities

# Shortcomings/Time Constraints #
### Testing ###
With the simplicity of this app and heavy reliance on abstraction/convention using Spring Boot components, I only wrote simple CRUD unit tests for a single repo. 
For more complex apps, I typically focus my unit testing on the service layer as pre-processing, validation, and post-processing grow in scope and complexity.
### Error Messaging ###
For all error messaging, I typically implement internationalized solutions to provide locale-relevant information to the application consumers. 
I deemed the time this would require to be beyond the scope of this exercise.